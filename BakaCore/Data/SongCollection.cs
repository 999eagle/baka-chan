using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using LiteDB;

using BakaCore.Data.Models;
using BakaCore.Music;
using BakaCore.Services;

namespace BakaCore.Data
{
	public class SongCollection
	{
		private ILiteCollection<SongData> collection;
		private ILiteStorage<string> fileStorage;
		private ILogger logger;
		private Configuration config;
		private IDictionary<string, ManualResetEvent> currentDownloads = new Dictionary<string, ManualResetEvent>();
		private object currentDownloadsLock = new object();

		internal SongCollection(LiteDatabase db, ILoggerFactory loggerFactory, Configuration config, IServiceProvider services)
		{
			logger = loggerFactory.CreateLogger<SongCollection>();
			collection = db.GetCollection<SongData>();
			collection.EnsureIndex(d => d.Id);
			collection.EnsureIndex(d => d.LastAccess);
			fileStorage = db.FileStorage;
			this.config = config;
		}

		public async Task<SongData> GetSong(string songId)
		{
			return await Task.Run(() =>
			{
				var song = collection.Find(d => d.Id == songId, 0, 1).FirstOrDefault();
				if (song == null) return null;
				if ((DateTime.Now - song.LastAccess) > config.Music.MaximumSongAgeTimeSpan)
				{
					collection.Delete(song.Id);
					fileStorage.Delete(song.FileId);
					return null;
				}
				song.LastAccess = DateTime.Now;
				collection.Update(song);
				return song;
			});
		}

		public Task CleanOldSongs()
		{
			return Task.Run(() =>
			{
				logger.LogInformation("Deleting old songs from database");
				var minimumLastAccessTime = DateTime.Now - config.Music.MaximumSongAgeTimeSpan;
				var oldSongs = collection.Find(d => d.LastAccess < minimumLastAccessTime).ToList();
				foreach(var song in oldSongs)
				{
					collection.Delete(song.Id);
					fileStorage.Delete(song.FileId);
				}
				logger.LogInformation($"Deleted {oldSongs.Count} songs");
			});
		}

		public async Task<Stream> GetOggStream(Song song)
		{
			var data = await GetSong(song.Id);
			if (data == null) return null;
			return await Task.Run(() =>
			{
				var dbStream = fileStorage.OpenRead(data.FileId);
				var buffer = new MemoryStream();
				dbStream.CopyTo(buffer);
				dbStream.Dispose();
				buffer.Position = 0;
				return buffer;
			});
		}
	}
}
