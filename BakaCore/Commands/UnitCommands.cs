using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;

using Discord;
using Discord.WebSocket;

using Units;

namespace BakaCore.Commands
{
	class UnitCommands
	{
		public UnitCommands()
		{
		}

		[Command("conv", Help = "Convert nearly arbitrary units of measurement.")]
		public async Task<bool> ConvertMeasurementCommand(SocketMessage message, [CustomUsageText("(<value> <from unit>)+ to (<to unit>)+")]string[] values)
		{
			var splitIdx = Array.IndexOf(values, "to");
			if (splitIdx <= 0)
			{
				return false;
			}

			var source = values.Take(splitIdx).ToArray();
			var target = values.Skip(splitIdx + 1).Select(t => Unit.TryParse(t, out var unit) ? (Unit?)unit : null).ToArray();
			if (target.Any(u => !u.HasValue))
			{
				return false;
			}
			var targetUnits = target.Select(u => u.Value).ToArray();
			if (targetUnits.Skip(1).Any(targetUnit => !targetUnit.CanConvertTo(targetUnits[0])))
			{
				return false;
			}

			Measurement? sourceMeasurement = null;
			for (var i = 0; i < source.Length - 1; i += 2)
			{
				if (!double.TryParse(source[i], out var val) || !Unit.TryParse(source[i + 1], out var unit))
				{
					return false;
				}

				var m = new Measurement((Ratio) val, unit);
				if (sourceMeasurement == null)
				{
					sourceMeasurement = m;
				}
				else if (sourceMeasurement.Value.Unit.CanConvertTo(unit))
				{
					sourceMeasurement += m;
				}
				else
				{
					return false;
				}
			}

			if (!sourceMeasurement.HasValue) return false;
			var measurement = sourceMeasurement.Value;

			if (!measurement.Unit.CanConvertTo(targetUnits[0])) return false;

			var targetString = "";
			foreach (var unit in targetUnits.SkipLast(1))
			{
				var newMeasurement = measurement.ConvertTo(unit);
				var val = (double) newMeasurement.Value;
				var intVal = Math.Truncate(val);
				if (intVal == 0) continue;
				targetString += $" {intVal} {unit.Name}";
				measurement = new Measurement((Ratio) (val - intVal), unit);
			}

			measurement = measurement.ConvertTo(targetUnits.Last());

			targetString += $" {measurement}";
			await message.Channel.SendMessageAsync($"{sourceMeasurement.Value} ={targetString}");
			return true;
		}
	}
}
