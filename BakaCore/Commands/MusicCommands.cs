using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;


using Discord;
using Discord.WebSocket;
using Concentus.Structs;
using Concentus.Oggfile;

using BakaCore.Music;
using BakaCore.Services;

namespace BakaCore.Commands
{
	class MusicCommands
	{
		private ILogger logger;
		private DiscordSocketClient client;
		private PlayerService playerService;
		private MusicService musicService;
		private Configuration config;

		public MusicCommands(IServiceProvider services)
		{
			var loggerFactory = services.GetRequiredService<ILoggerFactory>();
			logger = loggerFactory.CreateLogger<MusicCommands>();
			client = services.GetRequiredService<DiscordSocketClient>();
			musicService = services.GetRequiredService<MusicService>();
			playerService = services.GetRequiredService<PlayerService>();
			config = services.GetRequiredService<Configuration>();
		}

		private IVoiceChannel GetVoiceChannelFromMessage(SocketMessage message)
		{
			return (message.Author as IGuildUser)?.VoiceChannel;
		}

		private IGuild GetGuildFromMessage(SocketMessage message)
		{
			return (message.Channel as IGuildChannel)?.Guild;
		}

		private async Task<Song> LoadSong(string searchText, ISocketMessageChannel feedbackChannel = null)
		{
			searchText = searchText.Replace('`', '\'');
			if (feedbackChannel != null) await feedbackChannel.SendMessageAsync("No audio sources available");
			return null;
		}

		[Command("play", Scope = CommandScope.Guild)]
		public async Task PlayCommand(SocketMessage message, [FullText] string text)
		{
			var channel = GetVoiceChannelFromMessage(message);
			var player = playerService.GetPlayerForGuild(GetGuildFromMessage(message));
			if (channel == null && player?.VoiceChannel == null)
			{
				await message.Channel.SendMessageAsync("Please join a voice channel first and try again");
				await LoadSong(text); // start preloading song
				return;
			}
			var songTask = LoadSong(text, message.Channel)
				.ContinueWith((task, state) =>
				{
					var song = task.GetAwaiter().GetResult();
					if (song != null)
					{
						playerService.EnqueueAndPlay(channel, song.Id);
					}
				}, null);
			await Task.Delay(3000);
			player = playerService.GetPlayerForGuild(GetGuildFromMessage(message));
			if (!songTask.IsCompleted && (player == null || player.PlayerState == PlayerState.Disconnected))
			{
				await message.Channel.SendMessageAsync("Please wait a moment, the music will start soon");
			}
			await songTask;
		}

		[Command("stop", Scope = CommandScope.Guild)]
		public async Task StopCommand(SocketMessage message)
		{
			await playerService.StopPlayerInGuild(GetGuildFromMessage(message));
		}
	}
}
