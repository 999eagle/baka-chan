FROM microsoft/dotnet:2.1-runtime
LABEL maintainer="Sophie Tauchert <sophie@999eagle.moe>"
WORKDIR /app
COPY BakaChan/out/ ./
RUN echo "deb http://deb.debian.org/debian buster main" >> /etc/apt/sources.list && \
	apt-get update && \
	apt-get -y install libopus0/stable libc6/testing && \
	rm -r /var/lib/apt/lists/*
ENTRYPOINT ["./BakaChan.sh"]
