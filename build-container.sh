#!/usr/bin/env bash

# colors
bold=$(tput bold 2> /dev/null || echo -e "\x1b[1m")
red=$(tput setaf 1 2> /dev/null || echo -e "\x1b[38;5;1m")
green=$(tput setaf 2 2> /dev/null || echo -e "\x1b[38;5;2m")
yellow=$(tput setaf 3 2> /dev/null || echo -e "\x1b[38;5;3m")
blue=$(tput setaf 4 2> /dev/null || echo -e "\x1b[38;5;4m")
cyan=$(tput setaf 6 2> /dev/null || echo -e "\x1b[38;5;6m")
reset=$(tput sgr0 2> /dev/null || echo -e "\x1b[0m")

# misc. config
image_name="baka-chan"
if [[ "${CI_COMMIT_BRANCH}" = "master" ]]; then
	image_tag="latest"
else
	image_tag="${CI_COMMIT_REF_SLUG}"
fi

set -ex

echo "${green}Building container${reset}"
container=$(buildah from --pull mcr.microsoft.com/dotnet/core/runtime:3.1)
# stupid workaround because _apt uses 65534 (nogroup) by default, which doesn't work for rootless containers unless
# enough subgids are mapped
buildah run ${container} -- addgroup --gid 999 --force-badname _apt
buildah run ${container} -- usermod -g 999 _apt
buildah run ${container} -- apt-get update
buildah run ${container} -- apt-get -y install libopus0
buildah run ${container} -- bash -ec 'rm -r /var/lib/apt/lists/*'
buildah run ${container} -- mkdir -p /app
buildah copy ${container} bin /app
buildah config --workingdir "/app" --entrypoint '["./BakaChan.sh"]' --cmd '' ${container}

echo "${green}Committing container${reset}"
buildah commit --format docker ${container} ${image_name}
buildah rm ${container}

if [[ -n "${CI_REGISTRY}" ]]; then
	echo -e "${green}Pushing image${reset}"
	buildah push ${image_name} "${CI_REGISTRY_IMAGE}:${image_tag}"
fi
